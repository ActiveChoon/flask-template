import os

from flask_script import Manager
from flask_migrate import Migrate, upgrade, MigrateCommand
from application import create_app, db
from application.models import User, Role
from basedir import basedir

app = create_app(os.getenv('FLASK_CONFIG') or 'default')
migrate = Migrate(app, db)
manager = Manager(app)

manager.add_command("db", MigrateCommand)


@manager.shell
def make_shell_context():
    return dict(db=db, User=User, Role=Role)


@manager.command
def create_db():
    """Creates the db tables."""
    db.create_all()


@manager.command
def deploy():
    """Makes deployment task. Upgrades database tables, creates the db tables, roles and admin.
    And also it will create images dirs, if it does not exist. Loads environment variables from '.env' file."""
    if os.path.exists(os.path.join(basedir, ".env")):
        from dotenv import load_dotenv
        load_dotenv(dotenv_path=os.path.join(basedir, ".env"))
    upgrade()
    Role.insert_roles()
    User.insert_admin()


@manager.command
def destroy_db():
    """Drops the db tables."""
    db.drop_all()


if __name__ == "__main__":
    manager.run()

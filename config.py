import os
basedir = os.path.abspath(os.path.dirname(__file__))
from werkzeug.security import generate_password_hash


class Config:
    SECRET_KEY = os.environ.get('SECRET_KEY') or 'hard to guess string'
    SQLALCHEMY_TRACK_MODIFICATIONS = False
    SQLALCHEMY_RECORD_QUERIES = True
    APP_ROLES = ["Admin"]
    APP_MAIN_ADMIN = os.environ.get("ADMIN_LOGIN") or "admin"
    APP_MAIN_ADMIN_PASS_HASH = generate_password_hash(os.environ.get("ADMIN_PASS")) \
        if os.environ.get("ADMIN_PASS") else generate_password_hash("adminPass")
    APP_SLOW_DB_QUERY_TIME = 0.3
    APP_CONTENT_PER_PAGE = 30

    APP_MAIL_USERNAME = os.environ.get("APP_MAIL_USERNAME")
    APP_MAIL_PASSWORD = os.environ.get("APP_MAIL_PASSWORD")
    APP_MAIL_SERVER = os.environ.get("APP_MAIL_SERVER")
    APP_MAIL_PORT = os.environ.get("APP_MAIL_PORT")
    APP_MAIL_SENDER = os.environ.get("APP_MAIL_SENDER") or "Application logger"
    APP_ADMINS = os.environ.get("APP_MAIL_ADMINS")
    APP_MAIL_SUBJECT_PREFIX = os.environ.get("APP_MAIL_SUBJECT_PREFIX") or "LOG:"

    @staticmethod
    def init_app(app):
        pass


class DevelopmentConfig(Config):
    DEBUG = True
    SQLALCHEMY_DATABASE_URI = os.environ.get("TEST_DB")


class ProductionConfig(Config):
    DEBUG = False
    SQLALCHEMY_DATABASE_URI = os.environ.get("PROD_DB")

    @classmethod
    def init_app(cls, app):
        Config.init_app(app)
        import logging
        from logging.handlers import SMTPHandler
        credentials = None
        secure = None
        if getattr(cls, "MAIL_USERNAME", None) is not None:
            credentials = (cls.APP_MAIL_USERNAME, cls.APP_MAIL_PASSWORD)
            if getattr(cls, "MAIL_USE_TLS", None):
                secure = ()
        mail_handler = SMTPHandler(
            mailhost=(cls.APP_MAIL_SERVER, cls.APP_MAIL_PORT),
            fromaddr=cls.APP_MAIL_SENDER,
            toaddrs=cls.APP_ADMINS,
            subject=cls.APP_MAIL_SUBJECT_PREFIX + "Application error",
            credentials=credentials,
            secure=secure
        )
        mail_handler.setLevel(logging.ERROR)
        app.logger.addHandler(mail_handler)


class UnixConfig(ProductionConfig):
    @classmethod
    def init_app(cls, app):
        ProductionConfig.init_app(app)
        import logging
        from logging.handlers import SysLogHandler
        syslog_handler = SysLogHandler()
        syslog_handler.setLevel(logging.WARNING)
        app.logger.addHandler(syslog_handler)


config = {
    'development': DevelopmentConfig,
    'production': ProductionConfig,
    'default': DevelopmentConfig,
    'unix': UnixConfig
}

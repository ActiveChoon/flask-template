from flask_httpauth import HTTPBasicAuth
from flask import g, current_app, jsonify, request
from flask_sqlalchemy import get_debug_queries
from application.models import User
from .errors import forbidden, unauthorized
from . import api

auth = HTTPBasicAuth()


@auth.verify_password
def verify_password(username_or_token, password):
    if password == "":
        g.current_user = User.verify_auth_token(username_or_token)
        g.token_used = True
        return g.current_user is not None
    user = User.query.filter_by(username=username_or_token).first()
    if not user:
        return False
    g.current_user = user
    return user.verify_password(password)


@api.before_request
@auth.login_required
def before_request():
    if request.method in ['OPTIONS', ]:
        return
    if not g.current_user.isActive:
        forbidden("Unactivated account: Please, connect to the administrator")


@api.after_request
def after_request(response):
    for query in get_debug_queries():
        if query.duration >= current_app.config["VBB_APP_SLOW_DB_QUERY_TIME"]:
            current_app.logger.warning(
                "Slow query: {}\nParameters: {}\nDuration: {}\nContext: {}".format(
                    query.statement, query.parameters, query.duration, query.context))
    return response


@api.route("/ping/")
def ping():
    return "pong"


@auth.error_handler
def auth_error():
    return unauthorized("Invalid credentials")


@api.route("/token")
def get_token():
    try:
        if g.token_used:
            return unauthorized('Invalid credentials')
    except:
        return jsonify({"token": g.current_user.generate_auth_token(expiration=3600),
                        "expiration": 3600})

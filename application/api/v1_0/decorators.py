from flask import g
from .errors import forbidden
from functools import wraps


def admin_required(f):
    @wraps(f)
    def decorated_function(*args, **kwargs):
        if not g.current_user.is_admin():
            return forbidden("Insufficient permissions")
        return f(*args, **kwargs)
    return decorated_function

from werkzeug.security import generate_password_hash, check_password_hash
from flask import current_app
from flask_login import UserMixin
from itsdangerous import TimedJSONWebSignatureSerializer as Serializer
from application.exceptions import ValidationError
from . import db, LoginManager


class User(UserMixin, db.Model):
    __tablename__ = "users"
    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(64), unique=True, index=True)
    name = db.Column(db.String(128))
    roleId = db.Column(db.Integer, db.ForeignKey("roles.id"))
    passwordHash = db.Column(db.String(128))
    isActive = db.Column(db.Boolean, default=True)

    @property
    def password(self):
        raise AttributeError("Error: Password is not readable attribute")

    def activate(self):
        self.isActive = True

    @password.setter
    def password(self, password):
        self.passwordHash = generate_password_hash(password)

    def verify_password(self, password):
        return check_password_hash(self.passwordHash, password)

    def generate_auth_token(self, expiration):
        s = Serializer(current_app.config['SECRET_KEY'],
                       expires_in=expiration)
        return s.dumps({'id': self.id}).decode('utf-8')

    @staticmethod
    def verify_auth_token(token):
        s = Serializer(current_app.config['SECRET_KEY'])

        try:
            data = s.loads(token)
        except:
            return None
        return User.query.get(data['id'])

    def is_admin(self):
        return Role.query.get(self.roleId).name == current_app.config["VBB_APP_ROLES"][0]

    def to_json(self):
        user_json = {
            "id": self.id,
            "username": self.username,
            "name": self.name,
            "role": Role.query.get(self.roleId).name,
            "isActive": self.isActive
        }
        return user_json

    @staticmethod
    def from_json(user_json):
        if not (user_json.get("username") and user_json.get("name") and user_json.get("password")
                and user_json.get("role")):
            raise ValidationError("Not enough arguments")
        user = User(
            username=user_json.get("username"),
            name=user_json.get("name"),
            password=user_json.get("password"),
            roleId=Role.query.filter_by(name=user_json.get("role")).first().id,
            isActive=user_json.get("isActive") if user_json.get("isActive") is not None else True
        )
        return user

    @staticmethod
    def insert_admin():
        admin = User(
            username=current_app.config["APP_MAIN_ADMIN"],
            passwordHash=current_app.config["APP_MAIN_ADMIN_PASS_HASH"],
            roleId=Role.query.filter_by(name=current_app.config["APP_ROLES"][0]).first().id
        )
        db.session.add(admin)
        db.session.commit()
        print("Admin user created")


class Role(db.Model):
    __tablename__ = "roles"

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(64), unique=True)
    users = db.relationship("User", backref="role", lazy="dynamic")

    def __init__(self, **kwargs):
        super(Role, self).__init__(**kwargs)

    def __repr__(self):
        return "<Role {}>".format(self.name)

    @staticmethod
    def insert_roles():
        roles = current_app.config["APP_ROLES"]
        for role in roles:
            r = Role(name=role)
            db.session.add(r)
        db.session.commit()
        print("Roles created")

    def to_json(self):
        role_json = {
            "id": self.id,
            "name": self.name
        }
        return role_json

from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_login import LoginManager
from flask_cors import CORS
from config import config
from flask_apscheduler import APScheduler


db = SQLAlchemy()

loginManager = LoginManager()

cors = CORS(resources={r"/api/*": {"origins": "*"}})

scheduler = APScheduler()


def create_app(config_name):
    app = Flask(__name__)
    app.config.from_object(config[config_name])
    config[config_name].init_app(app)

    db.init_app(app)
    loginManager.init_app(app)
    cors.init_app(app)
    scheduler.init_app(app)
    scheduler.start()

    from .api.v1_0 import api as api_v_1_0_blueprint
    app.register_blueprint(api_v_1_0_blueprint, url_prefix="/api/v1.0")

    return app
